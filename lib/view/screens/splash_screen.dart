import 'package:flutter/material.dart';
import 'package:flutter_live_location_gmap/root_navigation.dart';

import 'onboardings.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    var d = Duration(seconds: 5);
    // Delayed 3 second to do something
    Future.delayed(d, () {
      Navigator.pushAndRemoveUntil(context,
          MaterialPageRoute(builder: (context) {
        return Onboardings();
      }), (route) => false);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              SizedBox(
                height: screenHeight * 0.35,
              ),
              Image.network(
                  'https://quench.mobi/assets/images/quench_logo_color.png'),
              //Logo(),
              Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}
