import 'package:flutter/material.dart';
import 'package:flutter_live_location_gmap/providers/covid_provider.dart';
import 'package:flutter_live_location_gmap/view/widgets/app_primary_button.dart';
import 'package:flutter_live_location_gmap/view/widgets/product_card.dart';
import 'package:provider/provider.dart';

import 'location_map.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
 

  @override
  Widget build(BuildContext context) {
    final Color _primaryColor = Color(0xFFe83694);
    return Scaffold(
      backgroundColor: Color(0xFFF5E4ED),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                child: Padding(
                  padding: const EdgeInsets.only(right: 8.0, top: 15),
                  child: AppPrimaryButton(
                    isLarge: true,
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => LocationMap()),
                      );
                    },
                    child: Text(
                      "Get Current Location",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.only(
                      left: 18, right: 18, top: 18, bottom: 18),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'New Arrival',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  )),
              Padding(
                padding: const EdgeInsets.only(
                    left: 18, right: 18, top: 25, bottom: 18),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    _renderNewArrivals(
                      "assets/images/account_background.jpg",
                      2000,
                      "Prix Camera 500SD",
                      () {
                        Navigator.of(context).pushNamed("/product");
                      },
                    ),
                    _renderNewArrivals(
                        "assets/images/ecomm1.jpg", 2000, "Prix Camera 500SD",
                        () {
                      Navigator.of(context).pushNamed("/product");
                    }),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 18, right: 18, top: 25, bottom: 18),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    _renderNewArrivals("assets/images/account_background.jpg",
                        2000, "Prix Camera 500SD", () {
                      Navigator.of(context).pushNamed("/product");
                    }),
                    _renderNewArrivals(
                        "assets/images/ecomm1.jpg", 2000, "Prix Camera 500SD",
                        () {
                      Navigator.of(context).pushNamed("/product");
                    }),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _renderNewArrivals(imageUrl, price, itemName, onTap) {
    return ProductCard(
        imageUrl: imageUrl, price: price, itemName: itemName, onTap: onTap);
  }
}
