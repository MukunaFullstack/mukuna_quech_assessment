import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProductCard extends StatelessWidget {
  final imageUrl;
  final price;
  final itemName;
  final Function onTap;

  const ProductCard(
      {Key key,
      @required this.imageUrl,
      @required this.price,
      @required this.itemName,
      @required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: this.onTap,
      child: Container(
        width: width / 2 - 25,
        height: 380,
        child: Card(
          color: Colors.grey[100],
          elevation: 6,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          child: Column(children: [
            ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(15.0)),
                child: Image.asset(this.imageUrl, fit: BoxFit.cover)),
            Container(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 12, bottom: 5, left: 5),
                      child: Text(this.itemName,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                          )),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 5.0),
                      child: Text("R ${this.price}",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                          )),
                    ),
                  ]),
            )
          ]),
        ),
      ),
    );
  }
}
