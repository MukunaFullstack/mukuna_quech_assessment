import 'package:flutter/material.dart';
import 'package:flutter_live_location_gmap/providers/covid_provider.dart';
import 'package:provider/provider.dart';
import 'package:timeago/timeago.dart' as timeago;

class GlobalStatistics extends StatelessWidget {
  //final GlobalSummaryModel summary;
  // GlobalStatistics({@required this.summary});

  @override
  Widget build(BuildContext context) {
    final covid = Provider.of<CovidProvider>(context, listen: false);
    covid.getGlobalSummary();
    return Column(
      children: <Widget>[
        buildCard("CONFIRMED", covid.globalSummary.totalConfirmed,
            covid.globalSummary.newConfirmed, Colors.blueAccent),
        buildCard(
            "ACTIVE",
            covid.globalSummary.totalConfirmed -
                covid.globalSummary.totalRecovered -
                covid.globalSummary.totalDeaths,
            covid.globalSummary.newConfirmed -
                covid.globalSummary.newRecovered -
                covid.globalSummary.newDeaths,
            Colors.red),
        buildCard("RECOVERED", covid.globalSummary.totalRecovered,
            covid.globalSummary.newRecovered, Colors.green),
        buildCard("DEATH", covid.globalSummary.totalDeaths,
            covid.globalSummary.newDeaths, Colors.black),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 4, vertical: 6),
          child: Text(
            "Statistics updated " + timeago.format(covid.globalSummary.date),
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 14,
            ),
          ),
        ),
      ],
    );
  }

  Widget buildCard(String title, int totalCount, int todayCount, Color color) {
    return Card(
      elevation: 1,
      child: Container(
        height: 100,
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        child: Column(
          children: <Widget>[
            Text(
              title,
              style: TextStyle(
                color: Colors.grey,
                fontWeight: FontWeight.bold,
                fontSize: 14,
              ),
            ),
            Expanded(
              child: Container(),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Total",
                      style: TextStyle(
                        color: color,
                        fontWeight: FontWeight.bold,
                        fontSize: 12,
                      ),
                    ),
                    Text(
                      totalCount.toString(),
                      style: TextStyle(
                        color: color,
                        fontWeight: FontWeight.bold,
                        fontSize: 28,
                      ),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      "Today",
                      style: TextStyle(
                        color: color,
                        fontWeight: FontWeight.bold,
                        fontSize: 12,
                      ),
                    ),
                    Text(
                      todayCount.toString(),
                      style: TextStyle(
                        color: color,
                        fontWeight: FontWeight.bold,
                        fontSize: 28,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
