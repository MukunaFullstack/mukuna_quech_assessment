import 'package:flutter/material.dart';

class AppPrimaryButton extends StatelessWidget {
  final Function onPressed;
  final String label;
  final Widget child;
  final bool isLarge;

  AppPrimaryButton({
    @required this.onPressed,
    this.child,
    this.label = "",
    this.isLarge = false,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: isLarge ? 318 : 150,
      height: 65,
      child: FlatButton(
        color: Theme.of(context).primaryColor,
        onPressed: onPressed,
        disabledColor: Colors.black,
        child: child != null
            ? child
            : Text(
                label,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
      ),
    );
  }
}
