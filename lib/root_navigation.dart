import 'package:flutter/material.dart';
import 'package:flutter_live_location_gmap/view/screens/cart_screen.dart';
import 'package:flutter_live_location_gmap/view/screens/contact_us_screen.dart';
import 'package:flutter_live_location_gmap/view/screens/home.dart';
import 'package:flutter_live_location_gmap/view/screens/covid_screen.dart';

import 'package:bottom_navy_bar/bottom_navy_bar.dart';

class RoootBottomNavigation extends StatefulWidget {
  @override
  _RoootBottomNavigationState createState() => _RoootBottomNavigationState();
}

class _RoootBottomNavigationState extends State<RoootBottomNavigation> {
  int _selectedIndex = 0;
  final List<Widget> _screens = [
    Home(),
    CovidScreen(),
    CartScreen(),
    ContactUsScreen()
  ];

  PageController _pageController = PageController();
  void _onPageChanged(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  void _onItemTapped(int index) {
    _pageController.animateToPage(
      index,
      duration: Duration(milliseconds: 200),
      curve: Curves.easeInOut,
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: SafeArea(
          child: PageView(
            controller: _pageController,
            onPageChanged: _onPageChanged,
            children: _screens,
          ),
        ),
        bottomNavigationBar: BottomNavyBar(
          onItemSelected: (index) => _onItemTapped(
            index,
          ),
          selectedIndex: _selectedIndex,
          showElevation: false,
          backgroundColor: Colors.transparent,
          items: [
            BottomNavyBarItem(
              textAlign: TextAlign.center,
              icon: Icon(
                Icons.home,
                color: _selectedIndex == 0
                    ? Theme.of(context).primaryColor
                    : Colors.blueGrey,
              ),
              title: Text(
                "Home",
                style: TextStyle(color: Theme.of(context).primaryColor),
              ),
              activeColor: Theme.of(context).primaryColor,
            ),
            BottomNavyBarItem(
              textAlign: TextAlign.center,
              icon: Icon(
                Icons.local_hospital,
                color: _selectedIndex == 1
                    ? Theme.of(context).primaryColor
                    : Colors.blueGrey,
              ),
              title: Text(
                "Covid",
                style: TextStyle(color: Theme.of(context).primaryColor),
              ),
              activeColor: Theme.of(context).primaryColor,
            ),
            BottomNavyBarItem(
              textAlign: TextAlign.center,
              icon: Icon(
                Icons.card_giftcard,
                color: _selectedIndex == 1
                    ? Theme.of(context).primaryColor
                    : Colors.blueGrey,
              ),
              title: Text(
                "Cart",
                style: TextStyle(color: Theme.of(context).primaryColor),
              ),
              activeColor: Theme.of(context).primaryColor,
            ),
            BottomNavyBarItem(
              textAlign: TextAlign.center,
              icon: Icon(
                Icons.chat,
                color: _selectedIndex == 1
                    ? Theme.of(context).primaryColor
                    : Colors.blueGrey,
              ),
              title: Text(
                "Chat",
                style: TextStyle(color: Theme.of(context).primaryColor),
              ),
              activeColor: Theme.of(context).primaryColor,
            ),
          ],
        ),
      ),
    );
  }
}
