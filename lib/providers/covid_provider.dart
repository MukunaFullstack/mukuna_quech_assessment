import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_live_location_gmap/Models/core/country.dart';
import 'package:flutter_live_location_gmap/Models/core/country_summary.dart';
import 'package:flutter_live_location_gmap/Models/core/global_summary.dart';
import 'package:flutter_live_location_gmap/Models/core/request_error.dart';
import 'package:flutter_live_location_gmap/services/covid_services.dart';

enum GetNewsStatus {
  NewsNotLoaded,
  NewsRequested,
  NewsLoadSuccess,
  NewsLoadFailure,
}

enum GetGlobalSummaryStatus {
  GlobalSummaryNotLoaded,
  GlobalSummaryRequested,
  GlobalSummaryLoadSuccess,
  GlobalSummaryLoadFailure,
}
enum GetCountrySummaryStatus {
  CountrySummaryNotLoaded,
  CountrySummaryRequested,
  CountrySummaryLoadSuccess,
  CountrySummaryLoadFailure,
}

class CovidProvider with ChangeNotifier {
  String _errorMessage;
  String get errorMessage => _errorMessage;

  GetNewsStatus _getNewsStatus = GetNewsStatus.NewsRequested;
  GetGlobalSummaryStatus _getGlobalSummaryStatus =
      GetGlobalSummaryStatus.GlobalSummaryRequested;
  GetCountrySummaryStatus _countrySummaryStatus =
      GetCountrySummaryStatus.CountrySummaryRequested;

  GlobalSummaryModel _globalSummary;
  List<CountrySummaryModel> _countrySummary = []; 

  GetNewsStatus get getNewsStatus => _getNewsStatus;
  GetGlobalSummaryStatus get getGlobalSummaryStatus => _getGlobalSummaryStatus;
  GetCountrySummaryStatus get getCountrySummaryStatus => _countrySummaryStatus;

  GlobalSummaryModel get globalSummary => _globalSummary;
  List<CountrySummaryModel> get countriesSummary => _countrySummary; 

  final _covidService = CovidServices();

  Future<Either<RequestError, GlobalSummaryModel>> getGlobalSummary() async {
    _getGlobalSummaryStatus = GetGlobalSummaryStatus.GlobalSummaryRequested;
    notifyListeners();

    final response = await _covidService.getGlobalSummary();

    if (response.isRight()) {
      _getGlobalSummaryStatus = GetGlobalSummaryStatus.GlobalSummaryLoadSuccess;
      notifyListeners();
    } else if (response.isLeft()) {
      _getGlobalSummaryStatus = GetGlobalSummaryStatus.GlobalSummaryLoadFailure;
      notifyListeners();
    }

    response.fold((left) {
      _errorMessage = left.message;
      print("LEFT MESSAGE IS $left");
      notifyListeners();
    }, (right) {
      _globalSummary = right;
      notifyListeners();
    });

    return response;
  }

  Future<Either<RequestError, List<CountrySummaryModel>>> getCountrySummary(
      String slug) async {
    _countrySummaryStatus = GetCountrySummaryStatus.CountrySummaryRequested;
    notifyListeners();

    final response = await _covidService.getCountrySummary(slug);

    if (response.isRight()) {
      _countrySummaryStatus = GetCountrySummaryStatus.CountrySummaryLoadSuccess;
      notifyListeners();
    } else if (response.isLeft()) {
      _countrySummaryStatus = GetCountrySummaryStatus.CountrySummaryLoadFailure;
      notifyListeners();
    }

    response.fold((left) {
      _errorMessage = left.message;
      print("LEFT MESSAGE IS $left");
      notifyListeners();
    }, (right) {
      _countrySummary = right;
      notifyListeners();
    });

    return response;
  }
}
