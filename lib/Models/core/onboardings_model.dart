class OnboardingsSlider {
  String imagePath;
  String title;
  String desc;

  OnboardingsSlider({this.imagePath, this.title, this.desc});

  void setImageAssetPath(String getImagepath) {
    imagePath = getImagepath;
  }

  void setTitle(String getTitle) {
    title = getTitle;
  }

  void setDesc(String getDesc) {
    desc = getDesc;
  }

  String getImageAssetPath() {
    return imagePath;
  }

  String getTitle() {
    return title;
  }

  String getDesc() {
    return desc;
  }
}

List<OnboardingsSlider> getSlider() {
  List<OnboardingsSlider> sliders = new List<OnboardingsSlider>();
  OnboardingsSlider onboardingsSlider = new OnboardingsSlider();
  //1
  onboardingsSlider.setImageAssetPath('assets/images/s3.png');
  onboardingsSlider.setTitle("Buy Food");
  onboardingsSlider
      .setDesc('Discover Quech your online shopping, our online shopping ');
  sliders.add(onboardingsSlider);

  onboardingsSlider = new OnboardingsSlider();

  //2
  onboardingsSlider.setImageAssetPath('assets/images/s2.png');
  onboardingsSlider.setTitle("Buy Electronics");
  onboardingsSlider
      .setDesc('Discover Quech your online shopping, our online shopping ');
  sliders.add(onboardingsSlider);

  onboardingsSlider = new OnboardingsSlider();

  //3
  onboardingsSlider.setImageAssetPath('assets/images/s1.png');
  onboardingsSlider.setTitle("Buy Medecine");
  onboardingsSlider
      .setDesc('Discover Quench your online shopping, our online shopping ');
  sliders.add(onboardingsSlider);
  onboardingsSlider = new OnboardingsSlider();

  return sliders;
}
