import 'package:flutter/material.dart';
import 'package:flutter_live_location_gmap/providers/covid_provider.dart';
import 'package:flutter_live_location_gmap/providers/location_provider.dart';
import 'package:flutter_live_location_gmap/root_navigation.dart';
import 'package:flutter_live_location_gmap/view/screens/home.dart';
import 'package:flutter_live_location_gmap/view/screens/splash_screen.dart';
import 'package:provider/provider.dart';

import 'view/screens/location_map.dart'; 

void main() {
  runApp(App());
}

class App extends StatelessWidget {
  final Color _primaryColor = Color(0xFFe83694);
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => LocationProvider(),
          child: LocationMap(),
        ),
        ChangeNotifierProvider(
          create: (context) => CovidProvider(),
          child: Home(),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          fontFamily: "Gilroy",
          primaryColor: _primaryColor,
          canvasColor: Colors.white,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          brightness: Brightness.light,
          buttonTheme: ButtonThemeData(
            buttonColor: _primaryColor,
            height: 50,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
          ),
        ),
        //home: LocationMap(),
        routes: {
          "/": (context) => SplashScreen(),
          "/location": (context) => LocationMap(),
          "/home-root": (context) => RoootBottomNavigation(),
        },
      ),
    );
  }
}
